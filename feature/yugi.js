const axios = require('axios');
const { API_KEY_RM_BG } = require('../config');
const Yugioh = async (text, msg) => {
   const mystring = text.replace('kartu yugi/','');
    if (mystring.length < 2) {
        return msg.reply('Format Salah. ketik *edit_bg/warna*');
    }

    axios
      .get(
        `https://db.ygoprodeck.com/api/v7/cardinfo.php?name=${mystring}`
      )
      .then(async (res) => {
        if (res.data.error) {
          msg.reply("No card matching your query was found in the database.");
        } else {
          const media = await MessageMedia.fromUrl(
            res.data.data[0].card_images[0].image_url
          );
          client.sendMessage(msg.from, media, {
            caption: `Name : ${res.data.data[0].name}\nType : ${res.data.data[0].type}\nDesc : ${res.data.data[0].desc}
            `,
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
    
    
    
}



module.exports = {
    Yugioh
}