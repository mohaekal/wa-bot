const { Client, LocalAuth, MessageMedia } = require("whatsapp-web.js");
const axios = require("axios");
const { EditPhotoHandler } = require('./feature/edit_foto');
const { Yugioh } = require('./feature/yugi');
const { ChatAIHandler } = require('./feature/ai');
const client = new Client({
  authStrategy: new LocalAuth(),
		puppeteer: {
		args: ['--no-sandbox'],
	}
});

client.initialize();

client.on("qr", (qr) => {
  console.log("QR RECEIVED", qr);
});

client.on("authenticated", () => {
  console.log("AUTHENTICATED");
});

client.on("ready", () => {
  console.log("Client is ready!");
});

client.on("message", async (msg) => {

 const text = msg.body.toLowerCase() || '';

    if (text.includes("edit foto dong kakek/")) {
        await EditPhotoHandler(text, msg);
    }


  if (text.includes("kartu yugi/")) {
  
    const mystring = text.replace('kartu yugi/','');
      
    axios
      .get(
        `https://db.ygoprodeck.com/api/v7/cardinfo.php?name=${mystring}`
      )
      .then(async (res) => {
        if (res.data.error) {
          msg.reply("No card matching your query was found in the database.");
        } else {
          const media = await MessageMedia.fromUrl(
            res.data.data[0].card_images[0].image_url
          );
          client.sendMessage(msg.from, media, {
            caption: `Name : ${res.data.data[0].name}\nType : ${res.data.data[0].type}\nDesc : ${res.data.data[0].desc}
            `,
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }
  
  
  
  
  
  if (text.includes("kakek ibu kota negara")) {
  
    const mystring = text.replace('kakek ibu kota negara ','');
      
    axios
      .get(
        `https://restcountries.com/v3.1/name/${mystring}`
      )
      .then(async (res) => {
        if (res.data.error) {
          msg.reply("No card matching your query was found in the database.");
        } else {
          const media = await MessageMedia.fromUrl(
            res.data[0].flags.png
          );
          client.sendMessage(msg.from, media, {
            caption: `Ibu Kota : ${res.data[0].capital}
            `,
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }
  
  
  if (text.includes("kakek jelasin negara")) {
  
    const mystring = text.replace('kakek jelasin negara ','');
      
    axios.get(
        `https://restcountries.com/v3.1/name/${mystring}`
      )
      .then(async (res) => {
        if (res.data.error) {
          msg.reply("No card matching your query was found in the database.");
        } else {
          const media = await MessageMedia.fromUrl(
            res.data[0].flags.png
          );
          client.sendMessage(msg.from, media, {
            caption: `Ibu Kota : ${res.data[0].capital}\nBenua : ${res.data[0].region}\nPopulasi : ${res.data[0].population}\nLokasi : ${res.data[0].maps.googleMaps}
            `,
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }
  
  
  
   if (text.includes("kakek rohman")) {

await ChatAIHandler(text, msg);
  
  
  
  }
  
  
  
});
